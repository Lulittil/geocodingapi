﻿using System;
using System.IO;
using System.Text;
using System.Text.Json.Serialization;

namespace GeocodingAPI.Models
{
    public class GeocodingData
    {
        public partial class Coordinates
        {
            [JsonPropertyName("lat")]
            public float lat { get; set; }
            [JsonPropertyName("lon")]
            public float lon { get; set; }

            public async void Logger(DataMode dataMode)
            {
                var builder = new StringBuilder();
                var path = AppDomain.CurrentDomain.BaseDirectory + "/logs.txt";

                if (dataMode == DataMode.Input)
                {
                    builder.Append($"[{DateTime.Now}] Введены данные: \n");
                }
                else
                {
                    builder.Append($"[{DateTime.Now}] Выведены данные: \n");
                }

                builder.Append("Lat: " + lat + "\n");
                builder.Append("Lon: " + lon + "\n");


                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    await writer.WriteAsync(builder);
                }
            }
        }

        public partial class Address
        {
            public string Country { get; set; }
            public string? Region { get; set; }
            public string City { get; set; }
            public string Street { get; set; }
            public string HomeNumber { get; set; }
            public string? FlatNumber { get; set; }

            public async void Logger(DataMode dataMode)
            {
                var builder = new StringBuilder();
                var path = AppDomain.CurrentDomain.BaseDirectory+"logs.txt";

                if (dataMode == DataMode.Input)
                {
                    builder.Append($"[{DateTime.Now}] Введены данные: \n");
                }
                else
                {
                    builder.Append($"[{DateTime.Now}] Выведены данные: \n");
                }

                builder.Append("Country: " + Country + "\n");
                builder.Append("Region: " + Region + "\n");
                builder.Append("City: " + City + "\n");
                builder.Append("Street: " + Street + "\n");
                builder.Append("HomeNumber: " + HomeNumber + "\n");
                builder.Append("FlatNumber: " + FlatNumber + "\n");

                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    await writer.WriteAsync(builder);
                }
            }


        }
        
        public enum DataMode
        {
            Output,
            Input
        }

    }
}
