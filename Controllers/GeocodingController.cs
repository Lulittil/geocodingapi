﻿using Microsoft.AspNetCore.Mvc;
using GeocodingAPI.Models;
using System.Threading.Tasks;

namespace GeocodingAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GeocodingController : ControllerBase
    {
        public GeocodingController()
        {

        }

        [HttpPost]
        public async Task<JsonResult> DefaultGeocoding(GeocodingData.Address fullAddress)
        {
            var result = await GeoParser.DefaultGeoParse(fullAddress);

            if (result == null)
            {
                Loggers.ErrorLogger.EmptyData();
                return new JsonResult(NotFound());
            }

            return new JsonResult(Ok(result));
        }

        [HttpPost]
        public async Task<JsonResult> ReverseGeocoding(GeocodingData.Coordinates coords)
        {
            var result = await GeoParser.ReverseGeoParsingAsync(coords);

            if (result == null)
            {
                Loggers.ErrorLogger.EmptyData();
                return new JsonResult(NotFound());
            }

            return new JsonResult(Ok(result));
        }
    }
}
