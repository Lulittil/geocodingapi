﻿using Dadata;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace GeocodingAPI.Models
{
    public static class GeoParser
    {
        public static async Task<List<GeocodingData.Coordinates>> DefaultGeoParse(GeocodingData.Address address)
        {
            address.Logger(GeocodingData.DataMode.Input);

            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, $"https://nominatim.openstreetmap.org/search?country={ address.Country}&city={address.City}&street={address.Street}&format=json&limit=1");
            request.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36");

            var response = await client.SendAsync(request);

            var result = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            var coords = JsonConvert.DeserializeObject<List<GeocodingData.Coordinates>>(result);

            coords[0].Logger(GeocodingData.DataMode.Output);

            return coords;
        }

        public static async Task<List<GeocodingData.Address>> ReverseGeoParsingAsync(GeocodingData.Coordinates coords)
        {
            coords.Logger(GeocodingData.DataMode.Input);

            var listResults = new List<GeocodingData.Address>();
            var token = "69ba372dfe9b3f7ef5f99678e57b04df597253ce";

            var api = new SuggestClientAsync(token);

            var response = await api.Geolocate(lat: coords.lat, lon: coords.lon, count: 10, radius_meters: 1000);
            var result = response.suggestions;

            foreach(var suggestion in result)
            {
                var newAddress = new GeocodingData.Address();
                newAddress.Country = suggestion.data.country;
                newAddress.Region = suggestion.data.region_with_type;
                newAddress.City = suggestion.data.city;
                newAddress.Street = suggestion.data.street_with_type;
                newAddress.HomeNumber = suggestion.data.house;
                newAddress.FlatNumber = suggestion.data.flat;
                listResults.Add(newAddress);
                newAddress.Logger(GeocodingData.DataMode.Output);
            }

            return listResults;
        }
       

    }
}
