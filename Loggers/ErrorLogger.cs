﻿using System;
using System.IO;

namespace GeocodingAPI.Loggers
{
    public static class ErrorLogger
    {
        private static string path = AppDomain.CurrentDomain.BaseDirectory + "/logs.txt";

        public static async void EmptyData()
        {
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                await writer.WriteAsync($"[{DateTime.Now}]Нет результатов по запросу \n");
            }
        }
    }
}
